# Copyright (C) 2014 TopCoder Inc., All Rights Reserved.

"""
This Python module provides function to perform the transcoding.

This module also provides the entry-point of the transcoder.

:version: 1.0
:author: albertwang, TCSASSEMBLER
"""
import psutil
import subprocess
import logging
import re
from ast import literal_eval
import urllib
import time
from datetime import datetime

import redis
import cherrypy
from apscheduler.schedulers.background import BackgroundScheduler
import psutil
from transcoding import *
from transcoding.util import MethodLogger, is_float, is_int
from settings import *


logger = logging.getLogger(__name__)  # logger for the current module

sched = BackgroundScheduler()  # create a background scheduler

# constants used internally

KEY_TRANSCODER_EXTRA_OPTS = '_transcoder_extra_opts_'

# the transcode options
TRANSCODE_OPTIONS = [
    # option name, option type, vlc option name (optional, if None it is the same as option name)
    ('venc', 'module', None),
    ('vcodec', 'str', None),
    ('vb', 'int', None),
    ('scale', 'float', None),
    ('fps', 'float', None),
    ('hurry_up', 'bool', 'hurry-up'),
    ('deinterlace', 'bool', None),
    ('width', 'int', None),
    ('height', 'int', None),
    ('maxwidth', 'int', None),
    ('maxheight', 'int', None),
    ('filters', 'str', None),
    ('aenc', 'module', None),
    ('acodec', 'str', None),
    ('ab', 'int', None),
    ('alang', 'str', None),
    ('channels', 'int', None),
    ('samplerate', 'int', None),
    ('audio_sync', 'bool', 'audio-sync'),
    ('afilters', 'str', None)
]


@cherrypy.popargs('source_url', 'target_format', 'reference_id')
class TranscodingWebService(object):
    """
    This class implements the "transcode" HTTP web service exposed by Transcoder

    :version: 1.0
    :author: albertwang, TCSASSEMBLER
    """

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @MethodLogger(logger)
    def transcode(self, reference_id, target_format, source_url, **options):
        """
        This method handles transcode request.

        The request is GET /transcode/{reference_id}/{source_url}/{target_format}?k1=v1&k2=v2...

        :param reference_id: the reference Id of the request
        :param target_format: the target format
        :param source_url: the source media URL
        :param options: the other options
        :return: the response as a dictionary, will be serialized to JSON by CherryPy.
        """

        logger.info("event=request_received_in_transcoder, ref_id=%s, timestamp=%s, transcoder=%s:%s",
                    reference_id, datetime.now().isoformat(), TRANSCODER_WEB_SERVICE_HOST, TRANSCODER_WEB_SERVICE_PORT)

        try:
            # delegate to transcode module function
            return {
                'reference_id': reference_id,
                'streamingURL': transcode(reference_id, target_format, source_url, options)
            }
        except (TypeError, ValueError) as e:
            logger.exception("Unable to process the request (400): reference_id=%s", reference_id)

            # Error with input, respond with 400
            cherrypy.response.status = 400
            return {'referenceId': reference_id, 'error': str(e)}

        except TranscoderOverloadedError as e:
            logger.exception("Unable to process the request (503): reference_id=%s", reference_id)

            cherrypy.response.status = 503
            return {'referenceId': reference_id, 'error': str(e)}

        except (TranscodingError, Exception) as e:
            logger.exception("Unable to process the request (500): reference_id=%s", reference_id)

            cherrypy.response.status = 500
            return {'referenceId': reference_id, 'error': str(e)}


def _get_concurrent_transcoding_sessions():
    """
    Get the number of concurrent transcoding sessions.

    :rtype: int
    :return: the number of concurrent sessions.
    """

    transcoding_sessions = 0

    for proc in psutil.process_iter():
        try:
            if proc.name() == VLC_BIN_NAME:
                transcoding_sessions += 1
        except psutil.AccessDenied:
            # sometimes we can not get the name of the process.
            pass

    return transcoding_sessions


def _merge_transcoding_options(transcoding_options, options):
    """
    Merge the options from query parameters into the transcoding options.

    :param transcoding_options: a string representation of the transcoding pipeline.
    :param options: the options from the query parameters
    :return: the string representaion of the merge transcoding pipeline.
    """

    # Parse default transcoding options into an array
    default_options = _parse_pipeline(transcoding_options)

    if 'none' in options:
        # none option specified, pass-through the transcoding
        default_options = [opt for opt in default_options if opt[0] != 'transcode']
    else:
        # Merge user overriding options
        transcode_options = next((opt[1] for opt in default_options if opt[0] == 'transcode'), None)
        for option_name, option_type, vlc_option_name in TRANSCODE_OPTIONS:
            if not vlc_option_name:
                vlc_option_name = option_name
            if option_name in options:
                if not transcode_options:  # create one if not exists
                    transcode_options = {}
                    default_options.append(('transcode', transcode_options))

                if option_type == 'module':
                    (module_name, module_opts) = _parse_module(options[option_name])
                    transcode_options[vlc_option_name] = (module_name, module_opts)
                elif option_type == 'bool':
                    transcode_options[vlc_option_name] = (True, {})
                else:
                    transcode_options[vlc_option_name] = (options[option_name], {})

    if 'std_mux' in options:
        std_options = next((opt[1] for opt in default_options if opt[0] == 'std'), None)
        if not std_options:  # create one if not exists
            std_options = {}
            default_options.append(('std', std_options))

        std_options['mux'] = (options['std_mux'], {})

    # Merge _transcoder_extra_opts_ is not None:
    if KEY_TRANSCODER_EXTRA_OPTS in options:
        extra_opts = _parse_pipeline(options[KEY_TRANSCODER_EXTRA_OPTS])
        _merge_pipelines(default_options, extra_opts)

    # Format default_options to string
    return _format_pipeline(default_options)


def _split_by(string, separator):
    """
    Split a string with given separator. {} pair and the inner content will be skipped.
    :param string: the given string
    :param separator: the separator
    :return: a list of strings separated.
    """

    last = 0
    level = 0
    for i in range(len(string)):
        if string[i] == '{':
            level += 1
        elif string[i] == '}':
            level -= 1
        elif string[i] == separator and level == 0:
            yield string[last:i]
            last = i + 1
    yield string[last:]


def _parse_pipeline(pipeline_str):
    """
    Parse the pipeline from string.
    :param pipeline_str: the string presentation of the pipeline
    :return: an array of modules
    """

    pipeline = []

    for module in _split_by(pipeline_str, ':'):
        # remove leading '#'
        (name, opts) = _parse_module(module.lstrip('#'))
        pipeline.append((name, opts))

    return pipeline


def _format_pipeline(pipeline):
    """
    Format the given pipeline into a string.
    :param pipeline: the pipeline (an array of modules)
    :return: the string representation of the given pipeline.
    """

    pipeline_strs = []
    for module_name, module_opts in pipeline:
        pipeline_strs.append(_format_module(module_name, module_opts))

    return '#' + ':'.join(pipeline_strs)


def _parse_module(module_str):
    """
    Parse a module string into (module_name, module_opts) pair. module_dict is a dict.
    :param module_str: the given module string.
    :return: the (module_name, module_opts) pair
    """

    idx = module_str.find('{')
    if idx < 0:
        return module_str, {}

    module_name = module_str[0:idx]
    module_opts = {}
    for key_value_pair in _split_by(module_str[idx + 1:-1], ','):
        parts = key_value_pair.split('=', 1)
        if len(parts) == 2:  # if it's key=value
            module_opts[parts[0]] = _parse_module(parts[1])
        else:  # if it's key only
            module_opts[parts[0]] = True, {}

    return module_name, module_opts


def _format_module(name, opts):
    """
    Format the module (name, opts) into a string.
    :param name: the name of the module
    :param opts: the module options. it is a dict.
    :return: the string representation of the module.
    """

    opt_strs = []
    for key, value in opts.iteritems():  # value is (module_name, module_opts)
        opt_str = key
        if value[0] is not True:
            opt_str += '=' + _format_module(value[0], value[1])
        opt_strs.append(opt_str)

    option_str = name
    if opt_strs:
        option_str += '{' + ','.join(opt_strs) + '}'

    return option_str


def _merge_pipelines(to_pipeline, from_pipeline):
    """
    Merge the pipeline from_opts into the pipeline to_opts.
    :param to_pipeline: the target pipeline of the merge.
    :param from_pipeline: the source pipeline of the merge.
    """

    for module_name, module_opts in from_pipeline:
        to_opt = next((opt for opt in to_pipeline if opt[0] == module_name), None)
        if not to_opt:  # append it if not exists in the target pipeline
            to_pipeline.append((module_name, module_opts))
        else:  # else merge the module
            _merge_module_opts(to_opt[1], module_opts)


def _merge_module_opts(to_opts, from_opts):
    """
    Merge the module options.
    :param to_opts: the target module options.
    :param from_opts: the source module options.
    """

    for key, module in from_opts.iteritems():
        if key not in to_opts:  # assign directly if not in target
            to_opts[key] = module
        else:
            to_module = to_opts[key]
            if module[0] == to_module[0]:  # if module name is the same, do the merge further.
                _merge_module_opts(to_module[1], module[1])
            else:
                to_opts[key] = module  # if module name is different, override the whole module.


def _transcode_validate_params(reference_id, target_format, source_url, options):
    """
    Validates the transcode parameters.

    :param reference_id: the reference ID of the request
    :param target_format: the target format
    :param source_url: the source media URL
    :param options: the options
    :raise TypeError: if any input parameter is not of required type
    :raise ValueError: if reference_id is None/empty, or source_url is None/empty
                or not HTTP/HTTPS/MMS/RTP/UDP protocol URL, or target_format is None/empty or not supported,
                or any valid option name maps to an invalid option value
                (refer to ADS section 'Transcoding Load Balancer Web Services' for option details).
    """

    if not reference_id:
        raise ValueError('reference_id is null or empty')
    if not isinstance(reference_id, str):
        raise TypeError('reference_id is not a string')
    if not re.match('[0-9a-f]{32}\Z', reference_id, re.I):
        raise TypeError('reference_id is not a uuid')

    if not source_url:
        raise ValueError('source_url is null or empty')
    if not isinstance(source_url, str):
        raise TypeError('source_url is not a string')

    allowed_protocols = ['http://', 'https://', 'mms://', 'rtp://', 'udp://']
    if not any(re.match(protocol, source_url, re.I) for protocol in allowed_protocols):
        raise ValueError('source_url is not HTTP/HTTPS/MMS/RTP/UDP protocol URL')

    if not target_format:
        raise ValueError('target_format is null or empty')
    if not isinstance(target_format, str):
        raise TypeError('target_format is not a string')

    if not target_format in SUPPORTED_TARGET_FORMATS:
        raise ValueError('target_format is not in %s' % SUPPORTED_TARGET_FORMATS)

    for option_name, option_type, vlc_option_name in TRANSCODE_OPTIONS:
        if option_name in options:
            if option_type in ['module', 'str']:
                if not options[option_name]:
                    raise ValueError('Query parameter %s is empty' % option_name)
            elif option_type == 'bool':
                pass
            elif option_type == 'int':
                if not is_int(options[option_name]):
                    raise TypeError('Query parameter %s is not int' % option_name)
            elif option_type == 'float':
                if not is_float(options[option_name]):
                    raise TypeError('Query parameter %s is not float' % option_name)


@MethodLogger(logger)
def transcode(reference_id, target_format, source_url, options):
    """
    Dispatch the transcoding request to a transcoder.

    :param reference_id: the reference ID of the request
    :param target_format: the target format
    :param source_url: the source media URL
    :param options: the options
    :rtype: str
    :return: the transcoding output streaming URL
    :raise TypeError: if any input parameter is not of required type
    :raise ValueError: if reference_id is None/empty, or source_url is None/empty
                or not HTTP/HTTPS/MMS/RTP/UDP protocol URL, or target_format is None/empty or not supported,
                or any valid option name maps to an invalid option value
                (refer to ADS section 'Transcoding Load Balancer Web Services' for option details).
    :raise TranscoderOverloadedError:
                if the maximum allowed simultaneous transcoding sessions has reached
    :raise TranscodingError:
                if there is something wrong with the transcoding process and the transcoding couldn't start
    """

    # Reject request if overloaded
    if _get_concurrent_transcoding_sessions() >= MAX_CONCURRENT_TRANSCODING_SESSIONS:
        raise TranscoderOverloadedError('Maximum allowed simultaneous transcoding sessions has reached.')

    # Validate parameters
    _transcode_validate_params(reference_id, target_format, source_url, options)

    # Prepare extra options
    if target_format == TARGET_FORMAT_HLS:
        index_folder = '{root_dir}/{reference_id}'.format(
            root_dir=HLS_OUTPUT_ROOT_DIRECTORY,
            reference_id=reference_id)

        index_file = '{index_folder}/{index_file}'.format(
            index_folder=index_folder,
            index_file=HLS_OUTPUT_INDEX_FILE)


        # HLS needs directory to store fragmented files
        streaming_url = HLS_STREAMING_SERVICE_URL_PATTERN.format(reference_id=reference_id)

        options[KEY_TRANSCODER_EXTRA_OPTS] = (
            'std{{' +
            'access=livehttp{{index={index_file},index-url={file_pattern}}},' +
            'dst={index_folder}/{file_pattern}' +
            '}}').format(index_file=index_file,
                         file_pattern=HLS_OUTPUT_FILE_PATTERN,
                         index_folder=index_folder)

        make_sure_path_exists(index_folder)

    elif target_format == TARGET_FORMAT_PSEUDO_HLS:
        index_folder = '{root_dir}/{reference_id}'.format(
            root_dir=HLS_OUTPUT_ROOT_DIRECTORY,
            reference_id=reference_id)

        index_file = '{index_folder}/{index_file}'.format(
            index_folder=index_folder,
            index_file=HLS_OUTPUT_INDEX_FILE)


        # Pseudo HLS also needs directory to store the only one segment
        streaming_url = HLS_STREAMING_SERVICE_URL_PATTERN.format(reference_id=reference_id)

        options[KEY_TRANSCODER_EXTRA_OPTS] = 'std{{dst={index_folder}/output.ts}}'.format(index_folder=index_folder)

        make_sure_path_exists(index_folder)

        # Also create a m3u8 manifest file in the output directory {root_dir}/{reference_id}/
        with open(index_file, 'w+') as logfile:  # print the cmd in the front of the log file
            logfile.write('#EXTM3U\n')
            logfile.write('#EXT-X-VERSION:3\n')
            logfile.write('#EXT-X-ALLOW-CACHE:NO\n')
            logfile.write('#EXT-X-MEDIA-SEQUENCE:1\n')
            logfile.write('#EXT-X-TARGETDURATION:10\n')
            logfile.write('#EXTINF:10,\n')
            logfile.write('output.ts\n')

    else:
        # it's looks like that vlc only supports live streaming instead of progressive mp4.
        # so we will generate the mp4 to a file and use apache2 to host it.

        index_folder = '{root_dir}/{reference_id}'.format(
            root_dir=MP4_OUTPUT_ROOT_DIRECTORY,
            reference_id=reference_id)

        index_file = '{index_folder}/{index_file}'.format(
            index_folder=index_folder,
            index_file=MP4_OUTPUT_INDEX_FILE)


        # HLS needs directory to store fragmented files
        streaming_url = MP4_STREAMING_SERVICE_URL_PATTERN.format(reference_id=reference_id)

        options[KEY_TRANSCODER_EXTRA_OPTS] = (
            'std{{' +
            'access=file,' +
            'dst={index_file}' +
            '}}').format(index_file=index_file)

        make_sure_path_exists(index_folder)


    # Merge options with default transcoding options
    default_options = TRANSCODING_OPTIONS.get(target_format, '')
    merged_transcoding_options = _merge_transcoding_options(default_options, options)

    if 'source_headers' in options:
        source_headers = urllib.quote(options['source_headers'], safe='')
        if '?' in source_url:
            source_url += '&source_headers=' + source_headers
        else:
            source_url += '?source_headers=' + source_headers

    try:
        logfile_name = os.path.join(TRANSCODING_EXECUTION_LOG_ROOT_DIRECTORY, reference_id + '.log')

        cmd = ('"{bin}" -v -I dummy --play-and-exit' +  # -v to enable verbose log
               ' --logfile "{logfile}" --file-logging' +
               ' "{source_url}"' +
               ' --sout "{opts}"').format(bin=VLC_BIN_PATH,
                                          logfile=logfile_name,
                                          source_url=source_url,
                                          opts=merged_transcoding_options)

        logger.info("VLC CMD: %s", cmd)

        with open(logfile_name, 'w+') as logfile:
            logfile.write(cmd + '\n\n')  # print the cmd in the front of the log file
            # Fork subprocess to execute VLC for transcoding
            vlc_process = subprocess.Popen(cmd, stdout=logfile, stderr=logfile, shell=True)  # stderr goes to logfile as well.

        logger.info("event=vlc_started_in_transcoder, ref_id=%s, timestamp=%s, trascoder=%s:%s",
                    reference_id, datetime.now().isoformat(), TRANSCODER_WEB_SERVICE_HOST, TRANSCODER_WEB_SERVICE_PORT)

        # Return streaming URL
        if target_format == TARGET_FORMAT_HLS:
            _wait_for_file_to_exist(vlc_process,index_file)  # just try to wait for the index file.
            return streaming_url
        elif target_format == TARGET_FORMAT_PSEUDO_HLS:
            _wait_for_file_to_exist(vlc_process,index_folder + '/output.ts')  # just try to wait for the output file.
            return streaming_url
        else:
            _wait_for_file_to_exist(vlc_process,index_file)  # just try to wait for the index file.
            return streaming_url
    except:
        raise TranscodingError("Failed to execute VLC for transcoding")


def kill(proc_pid): #kill process tree
    process = psutil.Process(proc_pid)
    for proc in process.get_children(recursive=True):
        proc.kill()
    process.kill()

def _wait_for_file_to_exist(vlc_process,file_path, seconds=1, repeats=50):
    """
    Wait until the file exists.

    :param file_path: the file
    :param seconds: the interval to check
    :param repeats: the number of times to check
    :return: True if the file exists, otherwise false.
    """
    for x in range(0, repeats):
        if ((vlc_process.poll() != None) and ( not os.path.exists(file_path)) ):
            break; #if vlc_process had died and no path is created, then raise error
        if os.path.exists(file_path):
            time.sleep(seconds / 4)  # wait for apache2 to recognize the file.
            return
        time.sleep(seconds)

    if (vlc_process.poll() == None):
        kill(vlc_process.pid) #kill process and its child process

    raise TranscodingError("Failed to generate the output file")


@sched.scheduled_job('interval', seconds=TRANSCODER_LOAD_STAT_REPORT_INTERVAL)
@MethodLogger(logger)
def report_load_statistics():
    """
    Report transcoder load statistics periodically.
    """

    try:
        transcoder_url = '{protocol}://{host}:{port}'.format(
            protocol='https' if TRANSCODER_WEB_SERVICE_SSL else 'http',
            host=TRANSCODER_WEB_SERVICE_HOST,
            port=TRANSCODER_WEB_SERVICE_PORT)

        r = redis.from_url(TRANSCODER_LOAD_STAT_REDIS_URL)
        transcoder_list = r.get(TRANSCODER_LOAD_STAT_TRANSCODERS_KEY)
        if transcoder_list is None:
            transcoder_list = []
        else:
            # literal_eval is needed, as the value from redis is always string.
            transcoder_list = literal_eval(transcoder_list)

        # append to transcoder list if not already present
        if transcoder_url not in transcoder_list:
            transcoder_list.append(transcoder_url)
            r.set(TRANSCODER_LOAD_STAT_TRANSCODERS_KEY, transcoder_list)

        r.set(transcoder_url, {
            'url': transcoder_url,
            'cpu_utilization': psutil.cpu_percent(),
            'ram_utilization': psutil.virtual_memory().percent,
            'transcoding_sessions': _get_concurrent_transcoding_sessions()
        })

        r.expire(transcoder_url, TRANSCODER_LOAD_STAT_TTL)
    except Exception:
        # log error
        logger.exception("Failed to report Load Statistics.")

# Entry-point of the transcoder
if __name__ == '__main__':
    # configure the log
    logging.basicConfig(level='DEBUG')

    # start job scheduler in the background.
    sched.start()

    cherrypy.config.update({
        'server.socket_host': TRANSCODER_WEB_SERVICE_HOST,
        'server.socket_port': TRANSCODER_WEB_SERVICE_PORT
    })

    if TRANSCODER_WEB_SERVICE_SSL:
        cherrypy.server.ssl_module = 'builtin'
        cherrypy.server.ssl_certificate = TRANSCODER_WEB_SERVICE_SSL_CERT
        cherrypy.server.ssl_private_key = TRANSCODER_WEB_SERVICE_SSL_KEY


    # start web service
    cherrypy.quickstart(TranscodingWebService(), '/')
