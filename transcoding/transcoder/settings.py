# Copyright (C) 2014 TopCoder Inc., All Rights Reserved.

"""
This Python module contains configurations(aka settings) of the Transcoder.

:version: 1.0
:author: albertwang, TCSASSEMBLER
"""

import os

from transcoding.util import make_sure_path_exists

# Represents the max concurrent transcoding sessions.
# Required.
# Positive integer.
MAX_CONCURRENT_TRANSCODING_SESSIONS = 3

# Represents the Transcoder Web Service port.
# Can be overridden from environment variable with the same name
# Required.
# Positive integer in range of [0, 65535]
TRANSCODER_WEB_SERVICE_PORT = 8081

if os.getenv('TRANSCODER_WEB_SERVICE_PORT'):
    TRANSCODER_WEB_SERVICE_PORT = int(os.getenv('TRANSCODER_WEB_SERVICE_PORT'))

# Represents the Transcoder Web Service host.
# Can be overridden from environment variable with the same name
# Required, non-empty.
# e.g. '192.168.1.101'
TRANSCODER_WEB_SERVICE_HOST = '127.0.0.1'

if os.getenv('TRANSCODER_WEB_SERVICE_HOST'):
    TRANSCODER_WEB_SERVICE_HOST = os.getenv('TRANSCODER_WEB_SERVICE_HOST')

# Indicates whether SSL is used for the Transcoder Web Service.
# Required. True/False
TRANSCODER_WEB_SERVICE_SSL = False

# SSL Settings. Refer to http://cherrypy.readthedocs.org/en/latest/deploy.html#ssl-support
# The SSL Server Certificate. Required if TRANSCODER_WEB_SERVICE_SSL is true.
TRANSCODER_WEB_SERVICE_SSL_CERT = './cert.pem'

# The SSL Server Private Key. Required if TRANSCODER_WEB_SERVICE_SSL is true.
TRANSCODER_WEB_SERVICE_SSL_KEY = './privkey.pem'

# Represents the URL of the Redis server that stores the Transcoder Load Statistics.
# Required, non-empty.
# e.g. redis://username:password@localhost:6379/0
TRANSCODER_LOAD_STAT_REDIS_URL = 'redis://localhost:6379/0'

# Represents the Redis server key for the value of a list of transcoder URLs.
# Required, non-empty.
# e.g. 'Transcoders_List'
TRANSCODER_LOAD_STAT_TRANSCODERS_KEY = 'Transcoders_List'

# Represents the interval for reporting Transcoder load statistics, in seconds.
# Required.
# Positive integer.
TRANSCODER_LOAD_STAT_REPORT_INTERVAL = 60

# Represents the TTL(Time-to-live) for a Transcoder load statistics, in seconds.
# Required.
# Positive integer.
TRANSCODER_LOAD_STAT_TTL = 90

# Represents the executable binary of the VLC.
# Required, non-empty.
# e.g. "vlc.exe".
VLC_BIN_NAME = 'vlc'

# Represents the full path of the executable binary of the VLC.
# Required, non-empty.
# e.g. "C:/Program Files (x86)/VideoLAN/VLC/vlc.exe".
VLC_BIN_PATH = '/usr/bin/vlc'

# CONSTANT: the progressive_mp4 format constant.
TARGET_FORMAT_PROGRESSIVE_MP4 = 'progressive_mp4'

# CONSTANT: Represents the hls format constant.
TARGET_FORMAT_HLS = 'hls'

# CONSTANT: Represents the pseudo_hls format constant.
TARGET_FORMAT_PSEUDO_HLS = 'pseudo_hls'

# Represents the supported target formats.
# Required.
# Non-empty string list, strings must be non-null non-empty.
# e.g. ['progressive_mp4','hls','pseudo_hls']
SUPPORTED_TARGET_FORMATS = [TARGET_FORMAT_PROGRESSIVE_MP4, TARGET_FORMAT_HLS, TARGET_FORMAT_PSEUDO_HLS]


# Represents the default VLC transcoding options.
# Required.
#
# Keys must be non-empty strings (as target format).
# Values must be non-empty strings (as VLC options).
TRANSCODING_OPTIONS = {

    TARGET_FORMAT_HLS: '#transcode{vcodec=h264,venc=x264{profile=baseline,level=30,keyint=10,ref=1}}:\
std{access=livehttp{seglen=10,delsegs=true,numsegs=30},\
mux=ts{use-key-frames}}',

    TARGET_FORMAT_PSEUDO_HLS: '#transcode{vcodec=h264,venc=x264{profile=baseline,level=30,keyint=10,ref=1}}:\
std{access=livehttp{seglen=10,delsegs=true,numsegs=30},\
mux=ts{use-key-frames}}',

    TARGET_FORMAT_PROGRESSIVE_MP4: '#transcode{vcodec=mp4v,vb=1024,acodec=mpga,ab=192}:\
std{mux=mp4}'

}

# Represents the root directory for storing HLS output files.
# Required, non-empty.
HLS_OUTPUT_ROOT_DIRECTORY = '/var/www/html/hls'

make_sure_path_exists(HLS_OUTPUT_ROOT_DIRECTORY)

# Represents the HLS output file pattern.
# Required, non-empty.
# e.g. 'output-######.ts'
HLS_OUTPUT_FILE_PATTERN = 'output-#######.ts'

# Represents the HLS index file name.
# Required, non-empty.
# e.g. 'list.m3u8'
HLS_OUTPUT_INDEX_FILE = 'list.m3u8'

# Represents the HLS streaming service URL pattern.
# Required, non-empty, should contain "{reference_id}" as the placeholder for reference ID.
HLS_STREAMING_SERVICE_URL_PATTERN = 'http://127.0.0.1:80/hls/{reference_id}/list.m3u8'


# Represents the root directory for storing MP4 output files.
# Required, non-empty.
MP4_OUTPUT_ROOT_DIRECTORY = '/var/www/html/mp4'

make_sure_path_exists(MP4_OUTPUT_ROOT_DIRECTORY)

# Represents the HLS index file name.
# Required, non-empty.
# e.g. 'list.m3u8'
MP4_OUTPUT_INDEX_FILE = 'video.mp4'

# Represents the MP4 streaming service URL pattern.
# Required, non-empty, should contain "{reference_id}" as the placeholder for reference ID.
MP4_STREAMING_SERVICE_URL_PATTERN = 'http://127.0.0.1:80/mp4/{reference_id}/video.mp4'


# Represents the root directory for storing transcoding logs.
# Required, non-empty.
TRANSCODING_EXECUTION_LOG_ROOT_DIRECTORY = '/var/log/transcoder'

make_sure_path_exists(TRANSCODING_EXECUTION_LOG_ROOT_DIRECTORY)
