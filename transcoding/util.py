# Copyright (C) 2014 TopCoder Inc., All Rights Reserved.

"""
This is the util module of the Transcoding Service.

:version: 1.0
:author: TCSASSEMBLER
"""

import os
import sys


class MethodLogger(object):
    """
    This is a Decorator class to log method entrance, exit and exception.

    :version: 1.0
    :author: TCSASSEMBLER
    """

    def __init__(self, logger):
        """
        Initializes a MethodLogger instance with the given logger.

        :param logger: the given logger
        :exception: ValueError if logger is null.
        """
        if logger is None:
            raise ValueError('logger is null')
        self.logger = logger

    def __call__(self, func):
        """
        Creates a decorator/wrapper of the given func.

        :param func: the given func.
        :return: a decorator/wrapper of the given func.
        """

        def wrapper(*args, **kwargs):
            self.logger.debug("Entering method %s: *args= %s; **kwargs= %s", func.__name__, args, kwargs)
            try:
                res = func(*args, **kwargs)
            except:
                self.logger.exception("Failed to execute method %s.", func.__name__)
                raise
            else:
                self.logger.debug("Exiting method %s: return= %s", func.__name__, res)
                return res

        return wrapper


def make_sure_path_exists(path):
    """
    Make sure the path exists
    :param path: the given path
    """

    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise


def is_float(x, min_value=0, max_value=sys.float_info.max):
    """
    Check if the string value is a float.
    :param x: the string value
    :param min_value: the min value
    :param max_value: the max value
    :return: Whether the string value is a float and in the range [min_value, max_value]
    """

    try:
        float_value = float(x)
    except ValueError:
        return False
    else:
        return min_value <= float_value <= max_value


def is_int(x, min_value=0, max_value=sys.maxint):
    """
    Check if the string value is an int.
    :param x: the string value
    :param min_value: the min value
    :param max_value: the max value
    :return: Whether the string value is an int and in the range [min_value, max_value]
    """

    try:
        float_value = float(x)
        int_value = int(float_value)
    except ValueError:
        return False
    else:
        return float_value == int_value and ('.' not in x) and min_value <= int_value <= max_value
