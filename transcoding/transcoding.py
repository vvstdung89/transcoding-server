# Copyright (C) 2014 TopCoder Inc., All Rights Reserved.

"""
This is the root package of the Transcoding Service.

This package also provides the definition of the application errors.

:version: 1.0
:author: albertwang, TCSASSEMBLER
"""

import traceback


class TranscodingError(Exception):
    """
    This exception indicates errors during transcoding process.

    :version: 1.0
    :author: albertwang, TCSASSEMBLER
    """

    def __init__(self, message=''):
        """
        Initializes the instance with an error message.

        :param self: the self instance.
        :param message: the error message. optional, defaults to ''
        """
        self.message = message
        self.traceback = traceback.format_exc()[:-1]

    def __str__(self):
        """
        Gets the string presentation of the exception with traceback

        :rtype: str
        :return: the string presentation of the exception.
        """
        if self.traceback == 'None':
            return self.message
        else:
            return self.message + '\n' + self.traceback


class TranscoderOverloadedError(Exception):
    """
    This exception indicates transcoder overloaded error.

    :version: 1.0
    :author: albertwang, TCSASSEMBLER
    """

    def __init__(self, message):
        """
        Initializes the instance with an error message.

        :param self: the self instance.
        :param message: the error message. optional, defaults to ''
        """
        self.message = message
        self.traceback = traceback.format_exc()[:-1]

    def __str__(self):
        """
        Gets the string presentation of the exception with traceback

        :rtype: str
        :return: the string presentation of the exception.
        """
        if self.traceback == 'None':
            return self.message
        else:
            return self.message + '\n' + self.traceback
