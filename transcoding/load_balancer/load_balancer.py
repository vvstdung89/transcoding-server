# Copyright (C) 2014 TopCoder Inc., All Rights Reserved.

"""
This Python module provides function to dispatch transcoding request to transcoders.

This module also provides the entry-point of the load balancer.

:version: 1.0
:author: albertwang, TCSASSEMBLER
"""
import re
import urllib
import uuid
import logging
import traceback
from ast import literal_eval
from datetime import datetime

import cherrypy
import redis
import requests
from transcoding import *
from transcoding.util import MethodLogger, is_int, is_float
from settings import *


logger = logging.getLogger(__name__)  # logger for the current module

# the transcode options
# Note that this may be difference from TRANSCODE_OPTIONS in transcoder,
# as the load balancer should choose to support less formats.
TRANSCODE_OPTIONS = [
    ('venc', 'module'),  # option name, option type
    ('vcodec', 'str'),
    ('vb', 'int'),
    ('scale', 'float'),
    ('fps', 'float'),
    ('hurry_up', 'bool'),
    ('deinterlace', 'bool'),
    ('width', 'int'),
    ('height', 'int'),
    ('maxwidth', 'int'),
    ('maxheight', 'int'),
    ('filters', 'str'),
    ('aenc', 'module'),
    ('acodec', 'str'),
    ('ab', 'int'),
    ('alang', 'str'),
    ('channels', 'int'),
    ('samplerate', 'int'),
    ('audio_sync', 'bool'),
    ('afilters', 'str')
]


class LoadBalancerError(Exception):
    """
    This exception indicate errors occurred at load balancer before the request being passed to a transcoder

    :version: 1.0
    :author: albertwang, TCSASSEMBLER
    """

    def __init__(self, message=''):
        """
        Initializes the instance with an error message.

        :param self: the self instance.
        :param message: the error message. optional, defaults to ''
        """
        self.message = message
        self.traceback = traceback.format_exc()[:-1]

    def __str__(self):
        """
        Gets the string presentation of the exception with traceback

        :rtype: str
        :return: the string presentation of the exception.
        """
        if self.traceback == 'None':
            return self.message
        else:
            return self.message + '\n' + self.traceback


@cherrypy.popargs('source_url', 'target_format')
class LoadBalancingWebService(object):
    """
    This class implements the "transcode" HTTP web service exposed by Transcoding Load Balancer.

    :version: 1.0
    :author: albertwang, TCSASSEMBLER
    """

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @MethodLogger(logger)
    def transcode(self, target_format, source_url, **options):
        """
        This method handles transcode request.

        :param target_format: the target format
        :param source_url: the source media URL
        :param options: the other options
        :return: the response as a dictionary, will be serialized to JSON by CherryPy.
        """

        # generate a reference ID for the request
        reference_id = uuid.uuid1().hex

        logger.info("event=request_received_in_load_balancer, ref_id=%s, timestamp=%s, load_balancer=%s:%s",
                    reference_id, datetime.now().isoformat(),
                    LOAD_BALANCING_WEB_SERVICE_HOST, LOAD_BALANCING_WEB_SERVICE_PORT)

        try:

            # delegate to dispatch_request module function
            streamingURL = dispatch_request(reference_id, target_format, source_url, options)
            raise cherrypy.HTTPRedirect(streamingURL, status=302)  # 302 redirect if succeeds.

        except cherrypy.HTTPRedirect:
            raise
        except (TypeError, ValueError) as e:
            logger.exception("Unable to process the request (400): reference_id=%s", reference_id)

            # Error with input, respond with 400
            cherrypy.response.status = 400
            return {'referenceId': reference_id, 'error': str(e)}

        except TranscoderOverloadedError as e:
            logger.exception("Unable to process the request (503): reference_id=%s", reference_id)

            cherrypy.response.status = 503
            return {'referenceId': reference_id, 'error': str(e)}

        except (LoadBalancerError, TranscodingError, Exception) as e:
            logger.exception("Unable to process the request (500): reference_id=%s", reference_id)

            cherrypy.response.status = 500
            return {'referenceId': reference_id, 'error': str(e)}


def _load_balancer_validate_params(target_format, source_url, options):
    """
    Validates the transcode parameters in load balancer.

    :param target_format: the target format
    :param source_url: the source media URL
    :param options: the options
    :raise TypeError: if any input parameter is not of required type
    :raise ValueError: if reference_id is None/empty, or source_url is None/empty
                or not HTTP/HTTPS/MMS/RTP/UDP protocol URL, or target_format is None/empty or not supported,
                or any valid option name maps to an invalid option value
                (refer to ADS section 'Transcoding Load Balancer Web Services' for option details).
    """

    if not source_url:
        raise ValueError('source_url is null or empty')
    if not isinstance(source_url, str):
        raise TypeError('source_url is not a string')

    allowed_protocols = ['http://', 'https://', 'mms://', 'rtp://', 'udp://']
    if not any(re.match(protocol, source_url, re.I) for protocol in allowed_protocols):
        raise ValueError('source_url is not HTTP/HTTPS/MMS/RTP/UDP protocol URL')

    if not target_format:
        raise ValueError('target_format is null or empty')
    if not isinstance(target_format, str):
        raise TypeError('target_format is not a string')

    if not target_format in SUPPORTED_TARGET_FORMATS:
        raise ValueError('target_format is not in %s' % SUPPORTED_TARGET_FORMATS)

    for option_name, option_type in TRANSCODE_OPTIONS:
        if option_name in options:
            if option_type in ['module', 'str']:
                if not options[option_name]:
                    raise ValueError('Query parameter %s is empty' % option_name)
            elif option_type == 'bool':
                pass
            elif option_type == 'int':
                if not is_int(options[option_name]):
                    raise TypeError('Query parameter %s is not int' % option_name)
            elif option_type == 'float':
                if not is_float(options[option_name]):
                    raise TypeError('Query parameter %s is not float' % option_name)


@MethodLogger(logger)
def dispatch_request(reference_id, target_format, source_url, options):
    """
    Dispatch the transcoding request to a transcoder.

    :param reference_id: the reference ID of the request
    :param target_format: the source media URL
    :param source_url: the target format
    :param options: the options
    :return: the transcoding output streaming URL
    :raise TypeError:
        if any input parameter is not of required type
    :raise ValueError:
        if reference_id is None/empty,
        or source_url is None/empty or not HTTP/HTTPS/MMS/RTP/UDP protocol URL,
        or target_format is None/empty or not supported,
        or any valid option name maps to an invalid option value
        (refer to ADS section 'Transcoding Load Balancer Web Services' for option details).
    :raise LoadBalancerError:
        if any error occurred at load balancer before the request being passed to a transcoder
    :raise TranscoderOverloadedError:
        if the number of 503 responses received by the balancer for this request has reached a maximum limit
    :raise TranscodingError:
        if the number of 500 responses received by the balancer for this request has reached a maximum limit

    """

    # Parameters validations
    _load_balancer_validate_params(target_format, source_url, options)

    # Query Redis for server load statistics
    transcoder_stat_list = []
    dead_transcoders = []

    try:
        r = redis.from_url(TRANSCODER_LOAD_STAT_REDIS_URL)
        transcoders = r.get(TRANSCODER_LOAD_STAT_TRANSCODERS_KEY)
        if transcoders is not None:
            transcoders = literal_eval(transcoders)
            for url in transcoders:
                stat = r.get(url)
                if stat:
                    stat = literal_eval(stat)
                    transcoder_stat_list.append(stat)
                else:
                    dead_transcoders.append(url)

        # remove dead transcoders
        if dead_transcoders:
            for url in dead_transcoders:
                transcoders.remove(url)
            r.set(TRANSCODER_LOAD_STAT_TRANSCODERS_KEY, transcoders)

        # sort statistics by transcoder load in ascending order,
        # the load is computed using settings.compute_transcode_load_score
        transcoder_stat_list.sort(key=compute_transcoder_load_score)
    except:
        raise LoadBalancerError('Unable to compute transcoder load score')

    # retry counters
    transcoding_error_retries = 0
    transcoding_overloaded_retries = 0

    # Try transcoders
    while (len(transcoder_stat_list) > 0
           and transcoding_error_retries < MAX_TRANSCODING_ERROR_RETRIES
           and transcoding_overloaded_retries < MAX_TRANSCODING_OVERLOADED_RETRIES):

        base_url = transcoder_stat_list.pop(0)['url']

        url = '{url}/transcode/{reference_id}/{target_format}/{source_url}/'.format(
            url=base_url,
            reference_id=reference_id,
            target_format=target_format,
            source_url=urllib.quote(source_url, safe=''))

        logger.info("dispatch request to %s", url)

        # dispatch request to the least busy transcoder
        try:
            transcoder_response = requests.get(url, params=options, verify=False)

            if transcoder_response.status_code == 200:
                # success response
                return transcoder_response.json()['streamingURL']
            elif transcoder_response.status_code == 400:
                # invalid request
                raise ValueError(transcoder_response.json()['error'])
            elif transcoder_response.status_code == 500:
                # transcoder server error
                transcoding_error_retries += 1
            elif transcoder_response.status_code == 503:
                # transcoder overloaded error
                transcoding_overloaded_retries += 1
        except ValueError:
            raise  # 400 error needs to be propagated
        except:
            # there was something wrong with the request(connection error, etc.),
            # log the error and continue to try another transcoder
            logger.exception("Failed to send request to %s. reference_id= %s", url, reference_id)

    if transcoding_error_retries == MAX_TRANSCODING_ERROR_RETRIES:
        raise TranscodingError("Maximum allowed transcoding errors has reached")
    else:
        raise TranscoderOverloadedError("No available transcoder services")


# Entry-point of the load balancer
if __name__ == '__main__':
    # configure the log
    logging.basicConfig(level='DEBUG')

    cherrypy.quickstart(LoadBalancingWebService(), '/', {
        'global': {
            'server.socket_host': LOAD_BALANCING_WEB_SERVICE_HOST,
            'server.socket_port': LOAD_BALANCING_WEB_SERVICE_PORT
        }
    })