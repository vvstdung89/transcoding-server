# Copyright (C) 2014 TopCoder Inc., All Rights Reserved.

"""
This Python module contains configurations(aka settings) of the Transcoding Load Balancer.

:version: 1.0
:author: albertwang, TCSASSEMBLER
"""

import os

# Represents the max transcoder overloaded error retries.
# Required.
# Positive integer.
MAX_TRANSCODING_OVERLOADED_RETRIES = 2


# Represents the max transcoding error retries.
# Required.
# Positive integer.
MAX_TRANSCODING_ERROR_RETRIES = 2


# Represents the URL of the Redis server that stores the Transcoder Load Statistics.
# Required, non-empty.
# e.g. redis://username:password@localhost:6379/0
TRANSCODER_LOAD_STAT_REDIS_URL = 'redis://localhost:6379/0'


# Represents the Redis server key for the value of a list of transcoder URLs.
# Required, non-empty.
# e.g. 'Transcoders_List'
TRANSCODER_LOAD_STAT_TRANSCODERS_KEY = 'Transcoders_List'

# Represents the Load Balancing Web Service port.
# Can be overridden from environment variable with the same name
# Required.
# Positive integer in range of [0, 65535]
LOAD_BALANCING_WEB_SERVICE_PORT = 8080

if os.getenv('LOAD_BALANCING_WEB_SERVICE_PORT'):
    LOAD_BALANCING_WEB_SERVICE_PORT = int(os.getenv('LOAD_BALANCING_WEB_SERVICE_PORT'))

# Represents the Load Balancing Web Service host.
# Can be overridden from environment variable with the same name
# Required, non-empty.
# e.g. '127.0.0.1'
LOAD_BALANCING_WEB_SERVICE_HOST = '127.0.0.1'

if os.getenv('LOAD_BALANCING_WEB_SERVICE_HOST'):
    LOAD_BALANCING_WEB_SERVICE_HOST = os.getenv('LOAD_BALANCING_WEB_SERVICE_HOST')

# CONSTANT: the progressive_mp4 format constant.
TARGET_FORMAT_PROGRESSIVE_MP4 = 'progressive_mp4'

# CONSTANT: Represents the hls format constant.
TARGET_FORMAT_HLS = 'hls'

# CONSTANT: Represents the pseudo_hls format constant.
TARGET_FORMAT_PSEUDO_HLS = 'pseudo_hls'

# Represents the supported target formats.
# Required.
# Non-empty string list, strings must be non-null non-empty.
# e.g. ['progressive_mp4','hls','pseudo_hls']
# NOTE that SUPPORTED_TARGET_FORMATS here may be less then the SUPPORTED_TARGET_FORMATS in transcoder
# as the load balancer should choose to support less formats.
SUPPORTED_TARGET_FORMATS = [TARGET_FORMAT_PROGRESSIVE_MP4, TARGET_FORMAT_HLS, TARGET_FORMAT_PSEUDO_HLS]

def compute_transcoder_load_score(stat):
    """
    Compute transcoder load score for a given statistics.

    :param stat: the dictionary that contains statistics
    :return: the score
    """

    return stat['cpu_utilization'] * 100000 + stat['ram_utilization'] * 10000 + stat['transcoding_sessions'] * 10